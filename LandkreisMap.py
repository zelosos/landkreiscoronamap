#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image
import sys
import csv
import wget
from datetime import date
from matplotlib.pyplot import cm
import json
import getopt

# 0 <= value <=1, defines the colorisation value
def scaleFactorContactRisk(value):
        """
        Normalize the value to be in range between 0 and 1
        This value can then be multiplied by the maximum to get the color value.
        This function normalizes the value for the contact risk value.
        """
        if value == 0:
                return 1

        # worst value = 1 in 100
        result = 100/value
        if result > 1:
                return 1
        return result

def scaleFactorIncidentsLast7Days(value):
        """
        Normalize the value to be in range between 0 and 1
        This value can then be multiplied by the maximum to get the color value.
        This function normalizes the value for the numbers of incidents in the last 7 days.
        """
        # worst value = 50 cases per 10k
        result = value/50
        if result > 1:
                return 1
        return result

def scaleFactorDelay(value):
        """
        Normalize the value to be in range between 0 and 1
        This value can then be multiplied by the maximum to get the color value.
        This function normalizes the value for the delay in days of the test been reported.
        """
        # worst value = 21 Days
        result = value/21
        if result > 1:
                return 1
        return result

def scaleFactorR(value):
        """
        Normalize the value to be in range between 0 and 1
        This value can then be multiplied by the maximum to get the color value.
        This function normalizes the value for the reproduction number R.
        """
        # worst value = 2
        result = value/2
        if result > 1:
                return 1
        return result

def colorizeImage(pixdata, value, pixelmap, land):
        """
        Given the pixel data of an image each pixel with alpha over 0 is been colorized.
        The color is been determend by the value. 0 represents min and 1 max.
        """
        # turn spectrum around
        value = (value*(-1))+1

        # get color value
        colorR = int(round(cm.Spectral(value)[0]*255))
        colorG = int(round(cm.Spectral(value)[1]*255))
        colorB = int(round(cm.Spectral(value)[2]*255))
        return colorizeImageRGB(pixdata, colorR, colorG, colorB, pixelmap, land)

def colorizeImageRGB(pixdata, R, G, B, pixelmap, land):
        """
        Given the pixel data of an image each pixel with alpha over 0 is been colorized.
        The color is been given as an RGB value with the variables R,G,B.
        """
        # colorise the image
        for pixel in pixelmap[land]:
                pixdata[pixel[0], pixel[1]] = (R, G, B, 255)
        return pixdata
                

def downloadData(download_enabled=True):
        """
        Download the data from the website of pavel mayer.
        The result is saved in the file data-<current date>.csv
        The resulting filename and the date is returned as a String.
        """
        # get date for filename
        today = date.today()
        dateStr = today.strftime("%Y-%m-%d")
        filename = "data-" + dateStr + ".csv"

        # get data
        if download_enabled:
                wget.download('https://pavelmayer.de/covid/risks/data.csv', filename)
        
        return filename, dateStr


"""
Here is the start of the main script.
"""

shortOptions = "di:o:"
longOptions = ["no-download", "input-csv=", "output-date="]
argumentList = sys.argv[1:]

try:
    arguments, values = getopt.getopt(argumentList, shortOptions, longOptions)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

# set default values
download_enabled = True
input_given = False
output_given = False

# Evaluate given options
for argument, value in arguments:
        if argument in ("-d", "--no-download"):
                download_enabled = False
        elif argument in ("-i", "--input"):
                input_given = True
                inputCSV = value
        elif argument in ("-o", "--output"):
                output_given = True
                outputDate = value

if input_given:
        if output_given:
                filename = inputCSV
                dateStr = outputDate
                download_enabled = False
        else:
                print("Output date is not given")
                sys.exit(2)
else:
        filename, dateStr = downloadData(download_enabled)

# open Landkreis Pixelmap
with open('landsPixelmap.json', 'r') as f:
    data = f.read()
landsPixelmap = json.loads(data)

# create new empty images
width = 2400
height = 3179
defaultColor = (0,0,0,0)
resultContactRisk = Image.new('RGBA', (width, height), color=defaultColor)
pixdataContactRisk = resultContactRisk.load()
resultIncidentsLast7Days = Image.new('RGBA', (width, height), color=defaultColor)
pixdataIncidentsLast7Days = resultIncidentsLast7Days.load()
resultReportDelay = Image.new('RGBA', (width, height), color=defaultColor)
pixdataReportDelay = resultReportDelay.load()
resultR = Image.new('RGBA', (width, height), color=defaultColor)
pixdataR = resultR.load()

# open data file
with open(filename, 'r', encoding="utf8") as csvfile:
        # read data
        data = csv.DictReader(csvfile, delimiter=',')
        for row in data:
                # if land has no representing image, confinue with next row
                land = row["Landkreis"]
                if not land in landsPixelmap.keys():
                        continue
 
                # add colorized image to contact risk map
                try:
                        value = float(row["Kontaktrisiko"])
                        pixdataContactRisk = colorizeImage(pixdataContactRisk, scaleFactorContactRisk(value), landsPixelmap, land)
                except:
                        pixdataContactRisk = colorizeImageRGB(pixdataContactRisk, 100, 100, 100, landsPixelmap, land)
                               
                # add colorized image to last incidents map
                try:
                        value = float(row["FaellePro100kLetzte7Tage"])
                        pixdataIncidentsLast7Days = colorizeImage(pixdataIncidentsLast7Days, scaleFactorIncidentsLast7Days(value), landsPixelmap, land)
                except:
                        pixdataIncidentsLast7Days = colorizeImageRGB(pixdataIncidentsLast7Days, 100, 100, 100, landsPixelmap, land)

                # add colorized image to delay map
                try:
                        value = float(row["DelayMean"])
                        pixdataReportDelay = colorizeImage(pixdataReportDelay, scaleFactorDelay(value), landsPixelmap, land)
                except:
                        pixdataReportDelay = colorizeImageRGB(pixdataReportDelay, 100, 100, 100, landsPixelmap, land)

                # add colorized image to R map
                try:
                        beforeLast7 = 1
                        if float(row["AnzahlFallLetzte7TageDavor"]) > 0:
                                beforeLast7 = float(row["AnzahlFallLetzte7TageDavor"])
                        value = float(row["AnzahlFallLetzte7Tage"])/beforeLast7
                        pixdataR = colorizeImage(pixdataR, scaleFactorR(value), landsPixelmap, land)
                except:
                        pixdataR = colorizeImageRGB(pixdataR, 100, 100, 100, landsPixelmap, land)
                
# save the result
resultContactRisk.save("Kontaktrisiko-" + dateStr + ".png")
resultIncidentsLast7Days.save("FaellePro100kLetzte7Tage-" + dateStr + ".png")
resultReportDelay.save("MeldeverzoegerungMittel-" + dateStr + ".png")
resultR.save("ReproduktionszahlR-" + dateStr + ".png")