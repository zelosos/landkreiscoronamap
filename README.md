# LandkreisCoronaMap

This Code creates a PNG Map for all German "Landkreise" out of the data from https://pavelmayer.de/covid/risks who uses the RKI as a source.

## Example for the reproduktion number R on 15.06.2020
![ReproduktionszahlR-2020-06-15](https://gitlab.com/zelosos/landkreiscoronamap/-/wikis/uploads/e8a8e0d6c40b30bfb60651a3452a237a/ReproduktionszahlR-2020-06-16.png)
- red = 2 or higher
- blue = 0